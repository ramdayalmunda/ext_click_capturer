!function () {
    console.log('popup', chrome);

    const api = axios.create({
        baseURL: "http://localhost:3039/api", // the base URL for nodeserver is okay but NGINX is giving error
        headers: {
            'Access-Control-Allow-Origin': '*', // Replace '*' with the origin you need
            'Content-Type': 'application/json',
        },
    });
    console.log('api',api)
    

    let origin = "http://localhost:3039"
    let recording = false;
    let loggedIn = false;
    let startBtn = document.getElementById('start-btn')
    let stopBtn = document.getElementById('stop-btn');
    let userDetailsDiv = document.getElementById('user-details')
    let userName = document.getElementById('user-name')
    let userEmail = document.getElementById('user-email')
    let myCapturesBtn = document.getElementById('my-captures-btn')
    myCapturesBtn.addEventListener('click',(e)=>{ window.open(origin+'/tutor-shot-list') })
    let userDetails = {};
    let inProgress = false;

    let loginBtn = document.getElementById('login-btn')
    loginBtn.addEventListener('click', () => { window.open(origin) })

    startBtn.addEventListener('click', async function () {
        if (inProgress){
            console.log('something in progress');
            return
        }
        inProgress = true
        try{
            console.log('apis', api)
            let { data } = await api.post('/tutor-shot', { title: "Some random title" }, { headers: { userId: userDetails.uid } } )
            console.log('creating reponse',data)
            if (data.success){
                chrome.runtime.sendMessage({ action: "startRecording", tutorShotId:data.tutorShot._id }, async function (response) {
                    recording = response.recording
        
                    console.log('to start. we need api', api)
        
        
        
                    modifyPopupUI()
                    inProgress = false
                })
            }
        }catch(err){
            console.log('error while starting to record', err)
            inProgress = false
        }

    })
    stopBtn.addEventListener('click', function () {

        if (inProgress){
            console.log('something in progress');
            return
        }
        inProgress = true
        chrome.runtime.sendMessage({ action: "stopRecording" }, function (response) {
            recording = response.recording
            modifyPopupUI()
            inProgress = false
        })
    })

    function modifyPopupUI() {
        console.log('modify ui', userDetails)
        loginBtn.style.display = loggedIn ? 'none' : ''
        startBtn.style.display = loggedIn ? (recording ? 'none' : '') : 'none'
        stopBtn.style.display = loggedIn ? (!recording ? 'none' : '') : 'none'
        if (userDetails.email && userDetails.uid){
            userDetailsDiv.style.display = 'block'
            userEmail.innerHTML = userDetails.email
            userName.innerHTML = userDetails.name
        }else{
            userDetailsDiv.style.display = 'none'
            userEmail.innerHTML = ""
            userName.innerHTML = ""
        }

    }


    function domLoadHandler() {
        chrome.runtime.sendMessage({ action: "checkStatus" }, function (response) {
            userDetails = response.userDetails;
            loggedIn = response.loggedIn,
                recording = response.recording
            modifyPopupUI()
        })
    }
    // this code should run everytime the popup is opened
    document.addEventListener("DOMContentLoaded", domLoadHandler);



}()
