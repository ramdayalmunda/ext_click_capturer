var getUserDetails = null

!function () {

    var tutorShotId = null
    var imageNumber = 0;
    var recording = false;
    var loggedIn = false;
    var userDetails = {}

    console.log('background.js registered', chrome)

    function onMessage(request, sender, sendResponse) {

        if (request.action === 'login') {
            console.log('B: to logged in', request.userDetails)
            userDetails = request.userDetails
            // check if credentials changed.
            if (request.userDetails.email != userDetails.email) {
                imageNumber = 0
            }
            loggedIn = true
            sendResponse({ message: "Logged In", success: true })
            return
        }

        if (request.action === 'checkStatus') {
            sendResponse({ userDetails, loggedIn, recording })
            return
        }

        if (loggedIn && request.action === 'startRecording') {
            imageNumber = 0
            recording = true;
            tutorShotId = request.tutorShotId
            sendResponse({ userDetails, loggedIn, recording });
            return
        }
        if (loggedIn && request.action === 'stopRecording') {
            recording = false;
            tutorShotId = null
            sendResponse({ userDetails, loggedIn, recording });
            return
        }

        if (request.action === 'captureSS') {
            if (!loggedIn) {
                console.log('not logged in for SS');
                return;
            }
            if (!recording) {
                console.log('recording not started for SS');
                return;
            }
            if (!tutorShotId){
                console.log('how to set not created');
                return
            }
            chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
                if (tabs && tabs.length) { // if any tab is active
                    var activeTab = tabs[0]
                    chrome.tabs.captureVisibleTab({ format: "png" }, function (dataUrl) {
                        let message = {
                            ...userDetails,
                            tutorShotOid: tutorShotId,
                            action: 'SSCaptured',
                            dataUrl: dataUrl, imageNumber: imageNumber++,
                            ...request.captureInfo ? request.captureInfo : {}
                        }
                        console.log('SS captured', message)
                        // to send message to the content instance of the
                        chrome.tabs.sendMessage(activeTab.id, message);
                    })
                }
            })
            return
        }


    }
    chrome.runtime.onMessage.addListener(onMessage)

    getUserDetails = function () {
        let dataSet = {
            userDetails: JSON.parse(JSON.stringify(userDetails)),
            recording: JSON.parse(JSON.stringify(recording)),
            loggedIn: JSON.parse(JSON.stringify(loggedIn)),
            tutorShotId: JSON.parse(JSON.stringify(tutorShotId)),
            imageNumber: JSON.parse(JSON.stringify(imageNumber)),
        }
        return dataSet
    }
}()
