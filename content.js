
var loggedIn = false;
!function () {
    let origin = "http://localhost:3039"
    if (window.origin == origin) login()
}()

function login() {

    let userDetails = {
        uid: localStorage.getItem('userId'),
        name: localStorage.getItem('name'),
        email: localStorage.getItem('email')
    }
    if (userDetails?.uid || userDetails?.email) { // can login
        console.log('logged in', userDetails)
        chrome.runtime.sendMessage({ action: "login", userDetails })
    }
}

var recording = false;
const api = axios.create({
    baseURL: "http://localhost:3039/api",
    headers: {
        'Access-Control-Allow-Origin': '*', // Replace '*' with the origin you need
        'Content-Type': 'application/json',
    },
});

window.addEventListener('mousedown', function (e) {
    if (e.button === 0) { // only left click allowed
        console.log('click received')

        let captureInfo = {
            targetDimensions: e.target.getBoundingClientRect(), // this is for the backdrop
            dimensions: { // this is for the main image
                height: document.body.clientHeight,
                width: document.body.clientWidth,
            },
            tagName: e.target.tagName,
            outerText: e.target.outerText,
            interText: e.target.interText,
            url: location.href,
            origin: location.origin,
        }
        chrome.runtime.sendMessage({ action: "captureSS", captureInfo })
    }

})
// content.js
chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {

    if (message.action == 'SSCaptured') {
        console.log('SSCaptured', message)
        api.post("/tutor-shot/screen-shot", message).then(res => {
            console.log('response', res)
        }).catch(err => {
            console.log('err', err)
        })
    }
    if (message.action == 'tabChanged') {
        console.log('tabChanged', message)
    }

});

